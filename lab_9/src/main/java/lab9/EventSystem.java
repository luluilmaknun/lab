package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.math.BigInteger;

/**
* Class representing event managing system
*/
public class EventSystem {
    /**
     * List of events
     */
    private ArrayList<Event> events;

    /**
     * List of users
     */
    private ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Find object Event by its name
     *
     * @param name
     * @return Event object
     */
    public Event findEvent(String name) {
        for (Event event : events) {
            if (event.getName().equals(name)) {
                return event;
            }
        }
        return null;
    }

    /**
     * Find object User by its name
     *
     * @param name
     * @return User object
     */
    public User getUser(String name) {
        for (User user : users) {
            if (user.getName().equals(name)) {
                return user;
            }
        }
        return null;
    }

    /**
     * Add Event object and time validation
     *
     * @return String of confirmation or validation
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        Event nowEvent = findEvent(name);

        if (nowEvent == null) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            Date startTime;
            Date endTime;
            try {
                startTime = df.parse(startTimeStr);
                endTime = df.parse(endTimeStr);
                if (endTime.before(startTime)) {
                    return "Waktu yang diinputkan tidak valid!";
                }
                BigInteger costPerHour = new BigInteger(costPerHourStr);

                events.add(new Event(name, startTime, endTime, costPerHour));
            } catch (ParseException e) { // catching error while parsing
                e.printStackTrace();
            }
            return "Event " + name + " berhasil ditambahkan!";
        }
        return "Event " + name + " sudah ada!";
    }

    /**
     * Add user to the system. Validate userName with users in system.
     *
     * @param name
     * @return String of confirmation or validation
     */
    public String addUser(String name) {
        User nowUser = getUser(name);

        if (nowUser == null) {
            users.add(new User(name));
            return "User " + name + " berhasil ditambahkan!";
        }
        return "User " + name + " sudah ada!";
    }

    /**
     * Add event to the system. Validate userName and eventName
     * with users and events in system
     *
     * @return String of confirmation ot validation
     */
    public String registerToEvent(String userName, String eventName) {
        Event event = findEvent(eventName);
        User user = getUser(userName);

        if (event == null && user == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (user == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (event == null) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        }

        if (user.addEvent(event)) {
            return user.getName() + " berencana menghadiri " + event.getName() + "!";
        }
        return user.getName() + " sibuk sehingga tidak dapat menghadiri eventName!";
    }

    /**
     * Get spesific event by its name
     * @return status of needed event
     */
    public String getEvent(String name) {
        Event event = findEvent(name);
        return event.toString();
    }
}