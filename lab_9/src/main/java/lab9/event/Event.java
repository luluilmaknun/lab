package lab9.event;

import java.util.Date;
import java.math.BigInteger;
import java.text.SimpleDateFormat;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;
    /** Event start time */
    private Date startTime;
    /** Event end time */
    private Date endTime;
    /** Event cost */
    private BigInteger costPerHour;

    /**
     * Constructor of class Event
     */
    public Event(String name, Date startTime, Date endTime, BigInteger costPerHour){
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.costPerHour = costPerHour;
    }
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }

    /**
     * Accessor for startTime field.
     * @return Date object of startTime Event
     */
    public Date getStartTime() { return startTime; }

    /**
     * Accessor for endTime field.
     * @return Date object of endTime Event
     */
    public Date getEndTime() { return endTime; }

    /**
     * Accessor for cost field.
     * @return num of the event cost
     */
    public BigInteger getCost() { return costPerHour; }

    /**
     * Override method Object toString()
     * Print Event status
     * @return
     */
    public String toString(){
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss"); // format print for date
        String startTimeStr = df.format(startTime);
        String endTimeStr = df.format(endTime);

        return name +
                "\nWaktu mulai: " + startTimeStr +
                "\nWaktu Selesai: " + endTimeStr +
                "\nBiaya kehadiran: " + costPerHour.toString(); // BigInteger to String
    }

    /**
     * Compare date of two event
     * @param anotherEvent (to be compared)
     * @return -1 if the event is held before anotherEvent
     *          0 if the event is held at the same time with anotherEvent
     *          1 if the event is held after anotherEvent
     */
    public int compareTo(Event anotherEvent){
        Date eventTime = startTime;
        Date anotherEventTime = anotherEvent.getStartTime();

        return eventTime.compareTo(anotherEventTime);
    }
}
