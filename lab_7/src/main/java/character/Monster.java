package character;

public class Monster extends Player{
    String roaring;
    public Monster(String name, int hp, String roaring){
        super(name, hp);
        setHp(2 * hp);
        this.roaring = roaring;
        setTipe("Monster");
        DAMAGED = 1;
    }

    public Monster(String name, int hp){
        super(name, hp);
        setHp(2 * hp);
        this.roaring = "AAAAAAaaaAAAAAaaaAAAAAA";
        setTipe("Monster");
        DAMAGED = 1;
    }

    public boolean canEat(Player anotherPlayer){
        if(anotherPlayer.isDead()){
            return true;
        }
        return false;
    }

    public String roar(){
        return roaring;
    }

    public String burn(Player anotherPlayer){
        return name + " tidak bisa burn";
    }
}
