package character;

public class Human extends Player{
    public Human(String name, int hp, String roaring){
        super(name, hp, roaring);
        setTipe("Human");
        DAMAGED = 1;
    }

    public Human(String name, int hp){
        super(name, hp);
        setTipe("Human");
        DAMAGED = 1;
    }

    public boolean canEat(Player anotherPlayer){
        if(anotherPlayer.getTipe().equals("Monster") && anotherPlayer.isDead() && anotherPlayer.isBurned()){
            return true;
        }
        return false;
    }

    public String roar(){
        return name + " tidak bisa berteriak";
    }

    public String burn(Player anotherPlayer){
        return name + " tidak bisa burn";
    }
}
