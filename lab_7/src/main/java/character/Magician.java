package character;

public class Magician extends Human{
    public Magician(String name, int hp, String roaring){
        super(name, hp, roaring);
        setTipe("Magician");
        DAMAGED = 2;
    }

    public Magician(String name, int hp){
        super(name, hp);
        setTipe("Magician");
        DAMAGED = 2;
    }

    public String roar(){
        return name + " tidak bisa berteriak";
    }

    public String burn(Player anotherPlayer){
        System.out.println(anotherPlayer.getTipe());
        anotherPlayer.setHp(anotherPlayer.getHp() - anotherPlayer.DAMAGED*10);
        if(anotherPlayer.isDead()){
            anotherPlayer.setBurned(true);
            return "Nyawa " + anotherPlayer.getName() + " " + anotherPlayer.getHp() +
                    "\ndan matang";
        }
        return "Nyawa " + anotherPlayer.getName() + " " + anotherPlayer.getHp();
    }
}
