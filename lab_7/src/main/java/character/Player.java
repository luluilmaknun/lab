package character;
import java.util.ArrayList;

public abstract class Player{
    protected String name;
    protected int hp;
    protected boolean burned;
    protected boolean eaten;
    protected boolean dead; //dunno if it is necessary
    protected String tipe;
    protected ArrayList<Player> diet;
    protected int DAMAGED;
    public static int MIN_HP = 0;

    public Player(String name, int hp){
        this.name = name;
        this.hp = hp;
        burned = false;
        eaten = false;
        isDead();
        diet = new ArrayList<Player>(0);
    }

    public Player(String name, int hp, String roaring){
        this.name = name;
        this.hp = hp;
        burned = false;
        eaten = false;
        isDead();
        diet = new ArrayList<Player>(0);
    }

    public String getTipe(){
        return tipe;
    }

    public void setTipe(String tipe){
        this.tipe = tipe;
    }

    public String getName(){
        return name;
    }

    public int getHp(){
        return hp;
    }

    public void setHp(int hp){
        this.hp = hp;
    }

    public boolean isDead(){
        if(hp <= MIN_HP){
            hp = 0;
            dead = true;
        }
        return dead;
    }

    public String printDead(boolean isDead){
        if(isDead){
            return "Sudah meninggal dunia dengan damai";
        }
        return "Masih hidup";
    }

    public void setDead(boolean dead){
        this.dead = dead;
    }

    public boolean isBurned(){
        return burned;
    }

    public void setBurned(boolean burned){
        this.burned = burned;
    }

    public boolean isEaten(){
        return eaten;
    }

    public ArrayList<Player> getDiet(){
        return diet;
    }

    public void setEaten(boolean eaten){
        this.eaten = eaten;
    }

    public void attack(Player anotherPlayer){
        anotherPlayer.setHp(anotherPlayer.getHp()- anotherPlayer.DAMAGED*10);
        anotherPlayer.isDead();
    }

    public void eat(Player anotherPlayer){
        if(canEat(anotherPlayer)){
            diet.add(anotherPlayer);
            anotherPlayer.setEaten(true);
            setHp(getHp() + 15);
        }
    }

    public abstract boolean canEat(Player anotherPlayer);
    public abstract String roar();
    public abstract String burn(Player anotherPlayer);
}
