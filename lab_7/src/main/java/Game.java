import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> players = new ArrayList<Player>();

    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for(Player player : players){
            if(player.getName().equals(name)){
                return player;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        Player player;
        if(find(chara) != null){
            return "Sudah ada karakter bernama " + chara;
        }
        if(tipe.equals("Human")){
            player = new Human(chara, hp);
        }else if(tipe.equals("Magician")){
            player = new Magician(chara, hp);
        }else{
            player = new Monster(chara, hp);
        }
        players.add(player);
        return chara + " ditambah ke game";
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        Player player;
        if(find(chara) != null){
            if(tipe.equals("human")){
                player = new Human(chara, hp, roar);
            }else if(tipe.equals("magician")){
                player = new Magician(chara, hp, roar);
            }else{
                player = new Monster(chara, hp, roar);
            }
            players.add(player);
            return chara + " ditambah ke game";
        }
        return "Sudah ada karakter bernama " + chara;
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        Player player = find(chara);
        if(player != null){
            players.remove(player);
            return chara + " dihapus dari game";
        }
        return "Tidak ada " + chara;

    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
     public String status(String chara){
        Player player = find(chara);
        String status = "";
        if(player != null){
            status += player.getTipe() + " " + player.getName() +
                      "\nHP: " + player.getHp() +
                      "\n" + player.printDead(player.isDead()) +
                      "\n" + diet(chara);
            return status;
        }
        return "Tidak ada " + chara;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String status = "";
        for(Player player: players){
            status += status(player.getName()) + "\n";
        }
        return status;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        String diets = "";
        try{
            Player player = find(chara);
            ArrayList<Player> diet = player.getDiet();
            if(diet.size() == 0){
                return "Belum memakan siapa siapa";
            }
            diets += "Memakan ";
            for(Player die: diet){
                diets += die.getTipe() + " " + die.getName() + " ";
            }
            return diets;
        }catch(NullPointerException n){return "Tidak ada " + chara;}
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String diets = "";
        for(Player player: players){
            ArrayList<Player> diet = player.getDiet();
            if(diet.size() == 0){
                return "Belum memakan siapa siapa";
            }
            diets += "Termakan ";
            for(Player die: diet){
                diets += die.getTipe() + " " + die.getName() + " ";
            }
        }
        return diets;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);
        if(me != null && enemy != null){
            me.attack(enemy);
            return "Nyawa " + enemyName + " " + enemy.getHp();
        }else if(me == null){
            return "Tidak ada " + meName;
        }
        return "Tidak ada " + enemyName;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);
        if(me != null && enemy != null){
            return me.burn(enemy);
        }else if(me == null){
            return "Tidak ada " + meName;
        }
        return "Tidak ada " + enemyName;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);
        if(me != null && enemy != null){
            me.eat(enemy);
            if(enemy.isEaten()){
                players.remove(enemy);
                return meName + " memakan " + enemyName +
                        "\nNyawa " + meName + " " + me.getHp();
            }
            return meName + " tidak bisa memakan " + enemyName;
        }else if(me == null){
            return "Tidak ada " + meName;
        }
        return "Tidak ada " + enemyName;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        try{
            String roar = find(meName).roar();
            return roar;
        }catch(NullPointerException n){
            return "Tidak ada " + meName;
        }
    }
}
