import java.util.Scanner;
import java.util.Arrays;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Lulu Ilmaknun Qurotaini, NPM 1706979341, Kelas ....., GitLab Account: .....
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);
		String nama, alamat, tanggalLahir, catatan;
		nama = alamat = tanggalLahir = catatan = "";
		short panjang, lebar, tinggi;
		panjang = lebar = tinggi = 0;
		byte makan, jumlahCetakan;
		makan = jumlahCetakan = 0;
		float berat;
		berat = 0;

		// TODO Bagian ini digunaSystem.out.println("WARNING: Keluarga ini tidak perlu direlokasi!"); System.exit(0);"
		// User Interface untuk meminta masukan
		try{
			System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
					"--------------------\n" +
					"Nama Kepala Keluarga   : ");
			nama = input.nextLine();
			System.out.print("Alamat Rumah           : ");
			alamat = input.nextLine();

			System.out.print("Panjang Tubuh (cm)     : ");
			panjang = input.nextShort();
			if(panjang > 250 || panjang < 0){System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!"); System.exit(0);}

			System.out.print("Lebar Tubuh (cm)       : ");
			lebar = input.nextShort();
			if(lebar > 250 || lebar < 0){System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!"); System.exit(0);}

			System.out.print("Tinggi Tubuh (cm)      : ");
			tinggi = input.nextShort();
			if(tinggi > 250 || tinggi < 0){System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!"); System.exit(0);}

			System.out.print("Berat Tubuh (kg)       : ");
			berat = input.nextFloat();
			if(berat > 150 || berat < 0){System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!"); System.exit(0);}

			System.out.print("Jumlah Anggota Keluarga: ");
			makan = input.nextByte();
			if(makan > 20 || makan < 0){System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!"); System.exit(0);}

			System.out.print("Tanggal Lahir          : ");
			tanggalLahir = input.next();
			input.nextLine();

			System.out.print("Catatan Tambahan       : ");
			catatan = input.nextLine();

			System.out.print("Jumlah Cetakan Data    : ");
			jumlahCetakan = input.nextByte();
			if(jumlahCetakan > 99 || jumlahCetakan < 0){System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!"); System.exit(0);}
		} catch (Exception r){System.out.println("WARNING: Kesalahan dalam input!"); System.exit(0);}

		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus int soal)
		int rasio = (int)((berat) / (float)((panjang * 0.01) * (lebar * 0.01) * (tinggi * 0.01)));
		input.nextLine();
		for (int i=0; i<jumlahCetakan; i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("\nPencetakan " + (i+1) + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			if (catatan.length() == 0) {catat = "Tidak ada catatan tambahan";}
			else {catat = "Catatan: " + catatan;}

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA SIAP DICETAK UNTUK " + penerima.toUpperCase() +
										 "\n--------------------\n" + nama + " - " + alamat +
										 "\nLahir pada tanggal " + tanggalLahir +
										 "\nRasio Berat Per Volume     = " + rasio + " kg/m^3\n" + catat;
			System.out.println(hasil);
		}

		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		int nomorKeluarga_num = (int)(panjang*tinggi*lebar);
		for (int i=0; i<nama.length();i++){
			nomorKeluarga_num += (int) nama.charAt(i);
		}
		nomorKeluarga_num = nomorKeluarga_num % 10000;

		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.charAt(0) + Integer.toString(nomorKeluarga_num);

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = (int) (50000 * 365 * makan);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		int tahunLahir = Integer.parseInt(tanggalLahir.substring(6,10)); // lihat hint jika bingung
		byte umur = (byte) (2018 - tahunLahir);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String namaApart = "";
		String kabupaten = "";
		if (umur <= 18){
			namaApart = "PPMT";
			kabupaten = "Rotunda";
		} else if (umur > 18 && anggaran < 100000000){
			namaApart = "Teksas";
			kabupaten = "Sastra";
		} else {
			namaApart = "Mares";
			kabupaten = "Margonda";
		}

		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "\nREKOMENDASI APARTEMEN\n" +
												 "--------------------" +
												 "\nMENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga +
												 "\nMENIMBANG:  Anggaran makanan tahunan: Rp " + anggaran +
												 "\n            Umur kepala keluarga: " + umur + " tahun" +
												 "\nMEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n" +
												 namaApart + ", kabupaten " + kabupaten;
		System.out.println(rekomendasi);

		input.close();
	}
}
