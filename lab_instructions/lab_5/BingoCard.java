import java.util.Scanner;

public class BingoCard {
	private Number[][] numbers;
	private Number[] numberStates;
	private boolean isBingo;
	private boolean repeat;
	private String nama;

	public BingoCard(String nama, Number[][] numbers, Number[] numberStates) {
		this.nama = nama;
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
		this.repeat = false;

	}

	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
		this.repeat = repeat;
	}

	public String getNama(){
		return nama;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public boolean isRepeat(){
		return repeat;
	}

	public void setRepeat(boolean repeat){
		this.repeat = repeat;
	}

	public String markNum(int num){
    String out = "";
    try{
	     if (numberStates[num].isChecked()) {out += num + " sebelumnya sudah tersilang";}
       else {numberStates[num].setChecked(true); out += num + " tersilang";}
    } catch(Exception r){out += "Kartu tidak memiliki angka " + num;}
	  checkWinning();
    if (isBingo){out += "\nBINGO!\n" + info();}
    return out;
	}

	public String info(){
    String papan = "";
    int counter = 0;
    for (Number[] number : numbers){
        for (Number num : number){
          if (numberStates[num.getValue()].isChecked()){
            papan += "| " + "X  ";
          }else{
            papan += "| " + numberStates[num.getValue()].getValue() + " ";
          }
        }
        papan += "|";
        counter++;
        if (counter < 5){papan += "\n";}
      }
		return papan;
	}

	public void restart(){
    for (Number[] number : numbers){
      for (Number num : number){
        numberStates[num.getValue()].setChecked(false);
      }
    }
		setRepeat(true);
		System.out.println("Mulligan!");
	}

  public void checkWinning(){
  	for (int i = 0; i< 5; i++){
      if ((numberStates[numbers[i][0].getValue()].isChecked() && numberStates[numbers[i][1].getValue()].isChecked() &&
           numberStates[numbers[i][2].getValue()].isChecked() && numberStates[numbers[i][3].getValue()].isChecked() &&
           numberStates[numbers[i][4].getValue()].isChecked()) ||
          (numberStates[numbers[0][i].getValue()].isChecked() && numberStates[numbers[1][i].getValue()].isChecked() &&
           numberStates[numbers[2][i].getValue()].isChecked() && numberStates[numbers[3][i].getValue()].isChecked() &&
           numberStates[numbers[4][i].getValue()].isChecked()) ||
          (numberStates[numbers[0][0].getValue()].isChecked() && numberStates[numbers[1][1].getValue()].isChecked() &&
           numberStates[numbers[2][2].getValue()].isChecked() && numberStates[numbers[3][3].getValue()].isChecked() &&
           numberStates[numbers[4][4].getValue()].isChecked()) ||
          (numberStates[numbers[0][4].getValue()].isChecked() && numberStates[numbers[1][3].getValue()].isChecked() &&
           numberStates[numbers[2][2].getValue()].isChecked() && numberStates[numbers[3][1].getValue()].isChecked() &&
           numberStates[numbers[4][0].getValue()].isChecked())){
        setBingo(true);
        break;
      }
    }
  }
}
