public class Manusia{
  private int umur;
  private String nama;
  private int uang;
  private float kebahagiaan;
  private boolean hidup = true;

  public static Manusia LASTHUMAN;

  public Manusia(String nama, int umur){
    this.nama = nama;
    this.umur = umur;
    uang = 50000;
    kebahagiaan = 50;
    LASTHUMAN = this;
  }

  public Manusia(String nama, int umur, int uang){
    this.nama = nama;
    this.umur = umur;
    this.uang = uang;
    kebahagiaan = 50;
    LASTHUMAN = this;
  }

  public String getNama(){return nama;}
  public int getUmur(){return umur;}
  public int getUang(){return uang;}
  public float getKebahagiaan(){return kebahagiaan;}
  public boolean masihHidup(){return hidup;}

  public void setNama(String nama){this.nama = nama;}
  public void setKebahagiaan(float kebahagiaan){this.kebahagiaan = kebahagiaan;}
  public void setUang(int uang){this.uang = uang;}

  public void cekKebahagiaan(){
    if(kebahagiaan >= 100){setKebahagiaan(100);}
    if(kebahagiaan <= 0){setKebahagiaan(0);}
  }

  public void beriUang(Manusia penerima){
    if(hidup == false){System.out.println(nama + " telah tiada");}
    else if(penerima.masihHidup() == false){System.out.println(penerima.getNama() + " telah tiada");}else{

    int jumlah = 0;
    for(int i = 0; i < penerima.getNama().length(); i++){
      jumlah += (int) penerima.getNama().charAt(i);
    }
    jumlah *= 100;

    if(jumlah <= uang){
      kebahagiaan += (jumlah/6000.0);
      float kebahagiaanPenerima = (float)(penerima.getKebahagiaan() + jumlah/6000.0);
      penerima.setKebahagiaan(kebahagiaanPenerima);
      penerima.setUang(penerima.getUang() + jumlah);
      uang -= jumlah;
      System.out.println(nama + " memberi uang sebanyak " + jumlah + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
    }else{
      System.out.println(nama + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
    }

    cekKebahagiaan();
    penerima.cekKebahagiaan();}
  }

  public void beriUang(Manusia penerima, int jumlah){
    if(hidup == false){System.out.println(nama + " telah tiada");}else if
    (penerima.masihHidup() == false){System.out.println(penerima.getNama() + " telah tiada");}else{

    if(jumlah <= uang){
      kebahagiaan += (jumlah/6000.0);
      float kebahagiaanPenerima = (float)(penerima.getKebahagiaan() + jumlah/6000.0);
      penerima.setKebahagiaan(kebahagiaanPenerima);
      penerima.setUang(penerima.getUang() + jumlah);
      uang -= jumlah;
      System.out.println(nama + " memberi uang sebanyak " + jumlah + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
    }else{
      System.out.println(nama + " ingin memberi uang kepada " + penerima.getNama() + " namun tidak memiliki cukup uang :'(");
    }
    cekKebahagiaan();
    penerima.cekKebahagiaan();}
  }

  public void bekerja(int durasi, int bebanKerja){
    if(hidup == false){System.out.println(nama + " telah tiada");}else{

    int bebanKerjaTotal = 0;
    int durasiBaru = 0;
    int pendapatan = 0;

    if(umur < 18){
      System.out.println(nama + " belum boleh bekerja karena masih di bawah umur D:");
    }else{
      bebanKerjaTotal = durasi * bebanKerja;

      if (bebanKerjaTotal <= kebahagiaan){
        kebahagiaan -= bebanKerjaTotal;
        pendapatan = (int) (bebanKerjaTotal * 10000);

        System.out.println(nama + " bekerja full time, total pendapatan: " + pendapatan);
      }else{
        durasiBaru = (int) (kebahagiaan/bebanKerja);
        bebanKerjaTotal = durasiBaru * bebanKerja;
        pendapatan = (int) (bebanKerjaTotal * 10000);
        kebahagiaan -= bebanKerjaTotal;
        System.out.println(nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan: " + pendapatan);
      }
      uang += pendapatan;
    }
    cekKebahagiaan();}
  }

  public void rekreasi(String namaTempat){
    if(hidup == false){System.out.println(nama + " telah tiada");}else{
    int biaya = namaTempat.length()*10000;

    if (uang >= biaya){
      uang -= biaya;
      kebahagiaan += namaTempat.length();
      System.out.println(nama + " berekreasi di " + namaTempat + ", " + nama + " senang :)");
    }else{
      System.out.println(nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat);
    }
    cekKebahagiaan();}
  }

  public void sakit(String namaPenyakit){
    if(hidup == false){System.out.println(nama + " telah tiada");}else{
    kebahagiaan -= namaPenyakit.length();
    System.out.println(nama + " terkena penyakit " + namaPenyakit + " :O");

    cekKebahagiaan();}
  }

  public void meninggal(){
    if (this == LASTHUMAN){System.out.print("Semua harta " + nama + " hangus\n");}
    else if(hidup == false){System.out.println(nama + " telah tiada");}else{

    System.out.println(nama + " meninggal dengan tenang, kebahagiaan: " + kebahagiaan);
    LASTHUMAN.setUang(LASTHUMAN.getUang() + uang);

    System.out.println("Semua harta " + nama + " disumbangkan untuk " + LASTHUMAN.getNama());
    hidup = false;}
    setUang(0);
  }

  public String toString(){
    String kalimat = "";
    if (hidup == true){
    kalimat = "Nama\t\t: " + nama +
            "\nUmur\t\t: " + umur +
            "\nUang\t\t: " + uang +
            "\nKebahagiaan\t: " + kebahagiaan;
    }else{
    kalimat = "Nama\t\t: Almarhum " + nama +
            "\nUmur\t\t: " + umur +
            "\nUang\t\t: " + uang +
            "\nKebahagiaan\t: " + kebahagiaan;
    }
    return kalimat;
  }
}
