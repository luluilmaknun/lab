// RABBITHOUSE KAK PEWE
// Author : LULU ILMAKNUN QUROTAINI
// NPM    : 1706979341

import java.util.Scanner;

public class RabbitHouse{
  static int bunMom = 1;

  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    String inp = input.nextLine();
    String type = inp.split(" ")[0].toLowerCase();
    String name = inp.split(" ")[1].toLowerCase();

    if (name.length() > 10 || name.length() < 0){
      System.out.println("Namanya panjang beud");
      return;
    }

    if (type.equals("normal")){
      System.out.println(bun(name.length()));
    }else if(type.equals("palindrom")){
      System.out.println(palin(name));
    }

    reset();
  }

  public static int bun(int len){
    if (len <= 1){
      return 1;
    }else{
      bunMom *= len;
      return bunMom + (bun(len-1));
    }
  }

  public static boolean isPalin(String name){
    if(name.length() <= 1){
      return true;
    }else if(name.charAt(0) == name.charAt(name.length() - 1)){
      return isPalin(name.substring(1, name.length() - 1));
    }else{
      return false;
    }
  }

  public static int palin(String name){
    String word = "";
    int child = 0;
    if (isPalin(name)){
      return child += 0;
    }else{
      child += 1;
      for (int i = 0;i< name.length(); i++){
        word = name.substring(0,i) + name.substring(i+1);
        child += palin(word);
      }
    }
    return child;
  }

  public static void reset(){
    bunMom = 1;
  }
}
