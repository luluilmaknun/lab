package xoxo;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Lulu Ilmaknun Qurotaini
 */
public class XoxoView extends JFrame {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    private JPanel textField;

    private JPanel buttonField;

    private SpringLayout layout;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        textField = new JPanel();
        buttonField = new JPanel();
        messageField = new JTextField(20);
        messageField.setPreferredSize(new Dimension(20, 60));
        keyField = new JTextField(20);
        seedField = new JTextField(20);
        logField = new JTextArea();
        logField.setPreferredSize(new Dimension(250,80));
        encryptButton = new JButton("Encrypt");
        decryptButton = new JButton("Decrypt");
        layout = new SpringLayout();

        textField.setLayout(layout);
        logField.setEditable(false);

        placeThings(new JLabel("Masukkan pesan anda:"),20, 10);
        placeThings(messageField, 160, 10);
        placeThings(new JLabel("Masukkan kunci :"), 20, 90);
        placeThings(keyField, 160, 90);
        placeThings(new JLabel("Masukkan seed :"), 20, 120);
        placeThings(seedField, 160, 120);
        placeThings(logField, 95, 150);

        buttonField.add(encryptButton);
        buttonField.add(decryptButton);
        add(buttonField, BorderLayout.PAGE_END);
        add(textField, BorderLayout.CENTER);
        setVisible(true);
        setSize(450,300);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    public void placeThings(Component component, int padLeft, int padTop){
        textField.add(component);

        layout.putConstraint(SpringLayout.WEST, component, padLeft, SpringLayout.WEST, textField);
        layout.putConstraint(SpringLayout.NORTH, component, padTop, SpringLayout.NORTH, textField);
    }

    public void resetLogField(){
        logField.setText("");
    }

    public void showMessageError(Exception excepion){
        JOptionPane.showMessageDialog(this, excepion.getMessage());
    }
}