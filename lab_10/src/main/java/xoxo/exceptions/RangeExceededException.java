package xoxo.exceptions;

/**
 * An exception that is thrown if the Seed contains
 * number not in range [0,36]
 */
public class RangeExceededException extends RuntimeException{
    public RangeExceededException(String msg){
        super(msg);
    }
}