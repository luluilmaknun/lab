package xoxo.exceptions;

/**
 * An exception that is thrown if the Message size
 * is more than 10 Kb
 */
public class SizeTooBigException extends RuntimeException{
    public SizeTooBigException(String msg){
        super(msg);
    }
}