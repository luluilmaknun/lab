package xoxo.exceptions;

/**
 * An exception that is thrown if the Kiss Key contains
 * a character that is not a-z, A-Z, or @.
 */
public class InvalidCharacterException extends RuntimeException{
    public InvalidCharacterException(String msg){
        super(msg);
    }
}