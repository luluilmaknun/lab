package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.key.HugKey;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setEncryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    gui.resetLogField();
                    XoxoEncryption encrypt = new XoxoEncryption(gui.getKeyText());
                    String message;
                    if(gui.getSeedText().equals("")){
                        message = encrypt.encrypt(gui.getMessageText()).getEncryptedMessage();
                    }else{
                        message = encrypt.encrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText())).getEncryptedMessage();
                    }
                    File file = new File(message + ".txt");
                    if(!file.exists()){
                        file.createNewFile();
                    }
                    FileWriter writer = new FileWriter(file);
                    writer.write(message);
                    writer.flush();
                    gui.appendLog("File created!");
                }catch (Exception r){
                    gui.showMessageError(r);
                }
            }
        });

        gui.setDecryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    gui.resetLogField();
                    XoxoDecryption decrypt = new XoxoDecryption(gui.getKeyText());
                    String message;
                    if(gui.getSeedText().equals("")){
                        message = decrypt.decrypt(gui.getMessageText(), HugKey.DEFAULT_SEED);
                    }else{
                        message = decrypt.decrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText()));
                    }
                    File file = new File(message + ".txt");
                    if(!file.exists()){
                        file.createNewFile();
                    }
                    FileWriter writer = new FileWriter(file);
                    writer.write(message);
                    writer.flush();
                    gui.appendLog("File created!");
                }catch (Exception r){
                    gui.showMessageError(r);
                }
            }
        });
    }
}