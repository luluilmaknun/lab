import java.util.Scanner;

public class SingleBingo {
  public static void main(String[] args){
    Number[][] numbers = new Number[5][5];
    Scanner input = new Scanner(System.in);

    for (int i = 0;i <5; i++){
      String[] line = input.nextLine().split(" ");
      int j = 0;
      for (String angka : line){
        numbers[i][j] = new Number(Integer.parseInt(angka), i, j);
        j++;
      }
    }

    Number[] states = new Number[100];
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
        states[numbers[i][j].getValue()] = numbers[i][j];
      }
    }

    BingoCard card = new BingoCard(numbers,states);

    while(!card.isBingo()){
      String[] perintah = input.nextLine().split(" ");
      if (perintah[0].toUpperCase().equals("MARK")){
        System.out.println(card.markNum(Integer.parseInt(perintah[1])));
      }else if(perintah[0].toUpperCase().equals("INFO")){
        System.out.println(card.info());
      }else if(perintah[0].toUpperCase().equals("RESTART")){
        card.restart();
      }else{
        System.out.println("Incorrect Command");
     }
    }
  }
}
