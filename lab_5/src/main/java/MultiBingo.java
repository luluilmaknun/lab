import java.util.*;

public class MultiBingo {
  public static void main(String[] args){
    ArrayList<BingoCard> players = new ArrayList<BingoCard>();
    Scanner input = new Scanner(System.in);

    String[] multi = input.nextLine().split(" ");
    int ord = 0;
    for (String player : multi){
      System.out.println("Masukkan kartu untuk player " + player);

      Number[][] numbers = new Number[5][5];
      for (int i = 0;i <5; i++){
        String[] line = input.nextLine().split(" ");
        int j = 0;
        for (String angka : line){
          numbers[i][j] = new Number(Integer.parseInt(angka), i, j);
          j++;
        }
      }

      Number[] states = new Number[100];
      for(int i=0; i<5; i++){
        for(int j=0; j<5; j++){
          states[numbers[i][j].getValue()] = numbers[i][j];
        }
      }

      players.add(new BingoCard(player, numbers, states));
      ord++;
    }

    while(true){
      String[] perintah = input.nextLine().split(" ");
      if (perintah[0].toUpperCase().equals("MARK")){
        for (BingoCard player : players){
          String text = "";
          text = player.getNama() + ": " + player.markNum(Integer.parseInt(perintah[1]));
          System.out.println(text);
        }
      }else if(perintah[0].toUpperCase().equals("INFO")){
        for (BingoCard player : players){
          if (player.getNama().equals(perintah[1])){
            System.out.println(player.getNama().toUpperCase());
            System.out.println(player.info());
          }
        }
      }else if(perintah[0].toUpperCase().equals("RESTART")){
        for (BingoCard player : players){
          if (player.getNama().equals(perintah[1])){
            if (player.isRepeat()){
              System.out.println("Udah pernah restart");
            }else{
              player.restart();
            }
          }
        }
      }else{
         System.out.println("Incorrect Command");
      }
    }
  }
}
