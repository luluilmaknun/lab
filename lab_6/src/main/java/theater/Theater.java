package theater;
import java.util.ArrayList;
import java.util.Arrays;
import movie.*;
import ticket.*;

public class Theater {
    String nama;
    Movie[] films;
    ArrayList<Ticket> tikets;
    long saldo;

    public Theater(String nama, long saldo, ArrayList<Ticket> tikets, Movie[] films){
        this.nama = nama;
        this.saldo = saldo;
        this.tikets = tikets;
        this.films = films;
    }

    public String getNama(){
        return nama;
    }

    public long getSaldo(){
        return saldo;
    }

    public void setSaldo(long saldo){
        this.saldo = saldo;
    }

    public Movie[] getFilms(){
        return films;
    }

    public Ticket cekTiket(String judul, String hari, boolean is3D){
        for (Ticket tiket : tikets){
            if(tiket.getFilm().getJudul().equals(judul) && tiket.getHari().equals(hari) && (tiket.get3D() == is3D)){
                return tiket;
            }
        }

        return null;
    }

    public Movie cekTiket(String judul){
        for (Ticket tiket : tikets){
            if(tiket.getFilm().getJudul().equals(judul)){
                return tiket.getFilm();
            }
        }

        return null;
    }

    public void printInfo(){
        String info = "";
        int i = 0;
        for(Movie film : films){
            i++;
            if(i == films.length){
                info += film.getJudul();
            }else{
                info += film.getJudul() + ", ";
            }
        }

        System.out.format("------------------------------------------------------------------" +
                        "\nBioskop\t\t\t: %s" +
                        "\nSaldo kas\t\t: %d" +
                        "\nJumlah tiket tersedia\t: %d" +
                        "\nDaftar film tersedia\t: %s" +
                        "\n------------------------------------------------------------------\n", nama, saldo, tikets.size(), info);
    }

    public static void printTotalRevenueEarned(Theater[] theaters){
        long totalSaldo = 0;
        String hasil = "Total uang yang dimiliki Koh Mas : Rp. %d" +
                       "\n------------------------------------------------------------------";

        for(Theater theater : theaters){
            hasil += "\nBioskop\t\t: " + theater.getNama() +
                     "\nSaldo Kas\t: Rp. " + theater.getSaldo() + "\n";
            totalSaldo += theater.getSaldo();
        }

        hasil+= "------------------------------------------------------------------\n";

        System.out.format(hasil, totalSaldo);
    }
}
