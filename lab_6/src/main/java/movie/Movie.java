package movie;

public class Movie {
    String judul;
    String rating;
    String genre;
    String jenis;
    int durasi;

    public Movie(String judul, String rating, int durasi, String genre, String jenis) {
        this.judul = judul;
        this.rating = rating;
        this.durasi = durasi;
        this.genre = genre;
        this.jenis = jenis;
    }

    public String getJudul(){
        return judul;
    }

    public String getRating(){
        return rating;
    }

    public void printInfo(){
        System.out.println("------------------------------------------------------------------" +
                           "\nJudul\t: " + judul +
                           "\nGenre\t: " + genre +
                           "\nDurasi\t: " + durasi +
                           "\nRating\t: " + rating +
                           "\nJenis\t: Film " + jenis +
                           "\n------------------------------------------------------------------\n");
    }
}
