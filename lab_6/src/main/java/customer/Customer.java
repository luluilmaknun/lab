package customer;
import theater.*;
import ticket.*;
import movie.*;

public class Customer {
    String nama;
    int umur;
    String jenisKelamin;

    public Customer(String nama, String jenisKelamin, int umur){
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.umur = umur;
    }

    public Ticket orderTicket(Theater bioskop, String movie, String hari, String is3DS){
        boolean is3D = false;
        Ticket newTicket;

        if (is3DS.equals("3 Dimensi")){
            is3D = true;
        }

        newTicket = bioskop.cekTiket(movie, hari, is3D);
        if (newTicket != null){
            if((newTicket.getFilm().getRating().equals("Remaja") && umur < 13) ||
               (newTicket.getFilm().getRating().equals("Dewasa") && umur < 17)){
                System.out.println(nama + " masih belum cukup umur untuk menonton " + newTicket.getFilm().getJudul() + " dengan rating " + newTicket.getFilm().getRating());
            }else{
                System.out.println(nama + " telah membeli tiket " + movie + " jenis " + is3DS + " di " + bioskop.getNama() + " pada hari " + hari + " seharga Rp. " + newTicket.getHarga());
                bioskop.setSaldo(bioskop.getSaldo() + newTicket.getHarga());
            }
        } else{
            System.out.println("Tiket untuk film " + movie + " jenis " + is3DS + " dengan jadwal " + hari + " tidak tersedia di " + bioskop.getNama());
        }

        return newTicket;
    }

    public void findMovie(Theater bioskop, String movie){
        if (bioskop.cekTiket(movie) != null){
            bioskop.cekTiket(movie).printInfo();
        } else {
            System.out.println("Film " + movie + " yang dicari " + nama +  " tidak ada di bioskop " + bioskop.getNama());
        }
    }
}
