package ticket;
import movie.*;
import theater.*;

public class Ticket {
    Theater bioskop;
    Movie film;
    String hari;
    boolean is3D;
    String is3DS;
    long harga;

    public Ticket(Movie film, String hari, boolean is3D){
        this.film = film;
        this.hari = hari;
        this.is3D = is3D;
        harga = 60000;

        if(hari.toLowerCase().equals("sabtu") || hari.toLowerCase().equals("minggu")){
            harga += 40000;
        }

        if(is3D){
            harga += harga*(0.2);
        }

        if(is3D){
            is3DS = "3 Dimensi";
        }else {is3DS = "Biasa";}
    }

    public boolean get3D(){
        return is3D;
    }

    public long getHarga(){
        return harga;
    }

    public Movie getFilm(){
        return film;
    }

    public String getHari(){
        return hari;
    }

    public void printInfo(){
        System.out.println("------------------------------------------------------------------" +
                           "\nFilm\t\t: " + film.getJudul() +
                           "\nJadwal Tayang\t: " + hari +
                           "\nJenis\t\t: " + is3DS +
                           "\n------------------------------------------------------------------\n");
    }
}
