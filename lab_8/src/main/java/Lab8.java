import java.util.Scanner;
import karyawan.*;

public class Lab8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        KaryawanController controller = new KaryawanController();

        int batas_gaji = input.nextInt();
        Staff.MAKS_GAJI = batas_gaji;

        while(true) {
            String[] perintah = input.nextLine().toUpperCase().split(" ");
            if(perintah[0].equals("TAMBAH_KARYAWAN")){
                System.out.println(controller.addKaryawan(perintah[1], perintah[2], Integer.parseInt(perintah[3])));
            }else if(perintah[0].equals("STATUS")){
                System.out.println(controller.statusKaryawan(perintah[1]));
            }else if(perintah[0].equals("TAMBAH_BAWAHAN")){
                System.out.println(controller.rekrut(perintah[1], perintah[2]));
            }else if(perintah[0].equals("GAJIAN")){
                System.out.println(controller.gajian());
            }
        }
    }
}
