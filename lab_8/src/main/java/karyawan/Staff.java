package karyawan;

public class Staff extends Manager {
    public static int MAKS_GAJI = 0;

    public Staff(String nama, String tipe, int gaji) {
        super(nama, tipe, gaji);
    }

    @Override
    public boolean bisaRekrut(Karyawan karyawan) {
        if(karyawan.getTipe().equals("INTERN") && bawahan.size() < 10){
            return true;
        }
        return false;
    }

    public boolean promosi(){
        if(getGaji() >= MAKS_GAJI){ return true; }
        return false;
    }
}
