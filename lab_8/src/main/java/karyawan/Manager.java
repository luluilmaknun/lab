package karyawan;

public class Manager extends Karyawan {
    public Manager(String nama, String tipe, int gaji) {
        super(nama, tipe, gaji);
    }

    @Override
    public boolean bisaRekrut(Karyawan karyawan) {
        if((karyawan.getTipe().equals("STAFF") || karyawan.getTipe().equals("INTERN")) && (bawahan.size() < 10)){
            return true;
        }
        return false;
    }
}
