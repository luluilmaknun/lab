package karyawan;

public class Intern extends Karyawan {
    public Intern(String nama, String tipe, int gaji) {
        super(nama, tipe, gaji);
    }

    @Override
    public boolean bisaRekrut(Karyawan karyawan) {
        return false;
    }
}
