package karyawan;
import java.util.ArrayList;

public abstract class Karyawan {
    private String nama;
    private String tipe;
    private int gaji;
    private int countGajian;
    protected ArrayList<Karyawan> bawahan = new ArrayList<Karyawan>();

    public Karyawan(String nama, String tipe, int gaji){
        this.nama = nama;
        this.tipe = tipe.toUpperCase();
        this.gaji = gaji;
    }

    public int getGaji() {
        return gaji;
    }

    public String getNama() {
        return nama;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public void tambahGaji(int gaji) {
        this.gaji += gaji;
    }

    public int gajianCount(){
        countGajian++;
        return countGajian;
    }

    public abstract boolean bisaRekrut(Karyawan karyawan);

    public String tambahBawahan(Karyawan karyawan){
        bawahan.add(karyawan);
        return "Karyawan " + karyawan.getNama() + " telah menjadi bawahan " + nama;
    }
}
