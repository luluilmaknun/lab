import karyawan.*;
import java.util.ArrayList;

public class KaryawanController{
    ArrayList<Karyawan> karyawans = new ArrayList<Karyawan>(0);

    public Karyawan findKaryawan(String nama){
        for(Karyawan karyawan : karyawans){
            if(karyawan.getNama().equals(nama)){ return karyawan; }
        }
        return null;
    }

    public String addKaryawan(String nama, String tipe, int gaji){
        Karyawan karyawan = findKaryawan(nama);
        if(karyawan == null){
            if(tipe.equals("MANAGER")){
                karyawans.add(new Manager(nama, tipe, gaji));
            }else if(tipe.equals("STAFF")){
                karyawans.add(new Staff(nama, tipe, gaji));
            }else if(tipe.equals("INTERN")){
                karyawans.add(new Intern(nama, tipe, gaji));
            }
            return nama + " mulai bekerja sebagai " + tipe + " di PT.TAMPAN";
        }
        return "Karyawan dengan nama " + nama + " telah terdaftar";
    }

    public String statusKaryawan(String nama){
        Karyawan karyawan = findKaryawan(nama);

        if(karyawan != null){
            return nama + " " + karyawan.getGaji();
        }
        return "Karyawan tidak ditemukan";
    }

    public String rekrut(String namaPerekrut, String namaDirekrut){
        Karyawan perekrut = findKaryawan(namaPerekrut);
        Karyawan direkrut = findKaryawan(namaDirekrut);

        if(perekrut == null || direkrut == null){
            return "Nama tidak berhasil ditemukan";
        }else if(perekrut.bisaRekrut(direkrut)){
            return perekrut.tambahBawahan(direkrut);
        }
        return "Anda tidak layak memiliki bawahan";
    }

    public String gajian(){
        String output = "Semua karyawan telah diberikan gaji";
        int i = 0;
        for(Karyawan karyawan: karyawans) {
            if(karyawan.gajianCount()%6 == 0){
                output += "\n" + naikGaji(karyawan);
                if(karyawan.getTipe().equals("STAFF")){
                    output += "\n" + promosi(i, karyawan);
                }
            }
            i++;
        }
        return output;
    }

    public String naikGaji(Karyawan karyawan){
        int gajiSebelum = karyawan.getGaji();
        int gajiSesudah = (int) (gajiSebelum * 0.1);
        karyawan.tambahGaji(gajiSesudah);
        return karyawan.getNama() + " mengalami kenaikan gaji sebesar 10% dari " + gajiSebelum + " menjadi " + karyawan.getGaji();
    }

    public String promosi(int i, Karyawan karyawan){
        Staff promosi = (Staff) karyawan;
        if(promosi.promosi()){
            karyawan.setTipe("MANAGER");
            karyawans.set(i, (Manager) karyawan);
            return "Selamat, " + karyawan.getNama() + " telah dipromosikan menjadi MANAGER";
        }
        return "";
    }
}
